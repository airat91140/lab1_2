#ifndef _TRACTRIX_H_
#define _TRACTRIX_H_

#include "Functions.h"

struct Line { //направляющая y=kx+b
    double k;
    double b;
};

class Tractrix {
private:
    double length = 1; //длина отрезка
    struct Line line = {0, 0};
public:
    Tractrix(double, double, double);

    Tractrix(double, double);

    explicit Tractrix(double);

    Tractrix();

    inline double get_length() const { return length; };

    inline struct Line get_line() const { return line; };

    void set_length(double);

    void set_line(double, double);

    struct Point get_coordinates(double&);

    double get_line_length(double);

    double get_radius(double) const;

    double get_area() const;

    double get_rotation_area() const;

    double get_volume() const;

};

#endif // !_TRACTRIX_H_
