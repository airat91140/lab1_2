#ifndef _FUNCTIONS_H_
#define _FUNCTIONS_H_

#include "Tractrix.h"

typedef struct Point {
    double x, y;
} Point;

class Tractrix;

double length(Point, Point);

    __declspec(selectany) const char *msgs[] = {"0. Quit",
                          "1. Set asymptote",
                          "2. Set length",
                          "3. Get asymptote",
                          "4. Get length",
                          "5. Get coordinates of rotation",
                          "6. Get length of rotation",
                          "7. Get radius",
                          "8. Get 2D area",
                          "9. Get 3D area",
                          "10. Get volume"};

    __declspec(selectany) const char *initmsgs[] = {"0. Quit",
                              "1. Set default asymptote(y=0) and length(1)",
                              "2. Set default asymptote(y=x) and set length manually",
                              "3. Set asymptote manually and and set default length(1)",
                              "4. Set asymptote and length manually"};

    __declspec(selectany) int NMsgs = sizeof(msgs) / sizeof(msgs[0]);
    __declspec(selectany) int InitNMsgs = sizeof(initmsgs) / sizeof(initmsgs[0]);

    const double PI = 3.14159265;

    template<class T>
    void get_num(T &);

    int dialog(const char*[], int);

    Tractrix *init(int);

    double get_angle(); 

    int SetAsympt(Tractrix &);

    int SetLen(Tractrix &);

    int GetAsympt(Tractrix &);

    int GetLength(Tractrix &);

    int GetCoordRotate(Tractrix &);

    int GetLenRotate(Tractrix &);

    int GetRad(Tractrix &);

    int Get2Area(Tractrix &);

    int Get3Area(Tractrix &);

    int GetVolume(Tractrix &);

    __declspec(selectany) int (*fptr[])(Tractrix &) = {nullptr, SetAsympt, SetLen, GetAsympt, GetLength, GetCoordRotate, GetLenRotate, GetRad,
                                 Get2Area, Get3Area, GetVolume};


#endif