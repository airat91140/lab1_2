#include <iostream>
#include "Tractrix.h"
#include "Functions.h"

using namespace std;

int main() {
    int rc;
    class Tractrix *tr = nullptr;
    cout << "Enter how do you want to initialize your tractrix" << endl;
    while ((rc = dialog(initmsgs, InitNMsgs))) {
        try {
            tr = init(rc);
            break;
        }
        catch (exception &br) {
            cout << "Oops, something bad happened. It seems to me that it is: " << br.what() << endl;
        }
    }
    cout << "Choose your variant" << endl;
    while (tr != nullptr && (rc = dialog(msgs, NMsgs))) {
        if (!fptr[rc](*tr))
            break;
    }
    delete tr;
    return 0;
}
