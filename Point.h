#ifndef _POINT_H_
#define _POINT_H_

#include "Functions.h"
#include "Tractrix.h"
class Point {
private:
    double x, y;
public:
    inline double get_x() const;

    inline double get_y() const;

    inline void set_x(double);

    inline void set_y(double);

    inline Point operator+(const Point &) const;

    inline Point operator-(const Point &) const;

    Point();
};

#endif