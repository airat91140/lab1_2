#include "Tractrix.h"
#include "Functions.h"
#include <iostream>
#include <cmath>

void Tractrix::set_length(double len) {
    if (len <= 0)
        throw std::exception("invalid length");
    this->length = len;
}

void Tractrix::set_line(double k, double b) {
    Tractrix::line.k = k;
    Tractrix::line.b = b;
}

Tractrix::Tractrix(double length, double k, double b) {
    if (length <= 0)
        throw std::exception("invalid length");
    Tractrix::length = length;
    Tractrix::line = {k, b};
}

Tractrix::Tractrix(double k, double b) {
    Tractrix::line = {k, b};
    Tractrix::length = 1;
}

Tractrix::Tractrix(double length) {
    if (length <= 0)
        throw std::invalid_argument("invalid length");
    Tractrix::length = length;
    Tractrix::line = {0, 0};
}

Tractrix::Tractrix() {
    Tractrix::length = 1;
    Tractrix::line = {0, 0};
}

Point Tractrix::get_coordinates(double& angle) {
    Point point;
    double x, y;
    if (angle <= 0 || angle >= PI)
        throw std::exception("Wrong angle");
    angle -= atan(this->line.k);
    angle = angle > 0 ? angle : -angle;
    x = (this->length * cos(angle) + this->length * log(tan(angle / 2.0)));
    y = (this->length * sin(angle)) + this->line.b;
    point.x = x;
    point.y = y;
    return point;
}

double Tractrix::get_line_length(double angle) {
    Point point1, point2;
    if (angle <= 0 || angle >= PI)
        throw std::exception("Wrong angle");
    point1 = get_coordinates(angle);
    point2.y = 0;
    point2.x = (point1.x - point1.y / tan(angle));
    double len = ::length(point1, point2);
    return len;
}

double Tractrix::get_radius(double angle) const {
    double rad;
    if (angle <= 0 || angle >= PI)
        throw std::exception("Wrong angle");
    angle -= atan(this->line.k);
    angle = angle > 0 ? angle : -angle;
    rad = this->length / tan(angle);
    return rad;
}

double Tractrix::get_area() const {
    return PI * this->length * this->length / 2;
}

double Tractrix::get_rotation_area() const {
    return PI * 4 * this->length * this->length;
}

double Tractrix::get_volume() const {
    return 2 * PI * pow(this->length, 3) / 3;
}