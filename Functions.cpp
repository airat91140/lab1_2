#include "Functions.h"
#include <iostream>
#include "Tractrix.h"
#include <cmath>
using namespace std;

double length(Point p1, Point p2) {
    Point p = { p1.x - p2.x, p1.y - p2.y };
    return sqrt(p.x * p.x + p.y * p.y);
}

template<class T>
void get_num(T &a) {
    while (true) {
        cin >> a;
        if (cin.good()) {
            return;
        }
        cout << "You are wrong. Repeat, please." << endl;
        cin.clear();
        cin.ignore(32767, '\n');
    }
}

double get_angle() {
    double angle = -1;
    get_num(angle);
    if (angle <= 0 || angle >= PI)
        throw exception("Wrong angle");
    return angle;
}

int dialog(const char *qmsgs[], int N) {
    char *errmsg = (char *) "";
    int rc;
    do {
        cout << errmsg;
        errmsg = (char *) "You are wrong. Repeat, please!\n";
        for (int i = 0; i < N; ++i)
            cout << qmsgs[i] << endl;
        cout << ("Make your choice: --> ") << endl;
        get_num(rc);
    } while (rc < 0 || rc >= N);
    return rc;
}

Tractrix *init(int rc) {
    Tractrix *tr;
    switch (rc) {
        case 0: {
            tr = nullptr;
            break;
        }
        case 1: {
            tr = new Tractrix;
            break;
        }
        case 2: {
            double len;
            cout << "Enter len:" << endl;
            get_num(len);
            tr = new Tractrix(len);
            break;
        }
        case 3: {
            double k, b;
            cout << "Enter k:" << endl;
            get_num(k);
            cout << "Enter b:" << endl;
            get_num(b);
            tr = new Tractrix(k, b);
            break;
        }
        case 4: {
            double k, b, len;
            cout << "Enter k:" << endl;
            get_num(k);
            cout << "Enter b:" << endl;
            get_num(b);
            cout << "Enter len:" << endl;
            get_num(len);
            tr = new Tractrix(len, k, b);
            break;
        }
        default:
            tr = nullptr;
    }
    return tr;
}

int SetAsympt(Tractrix &tr) {
    double k, b;
    cout << "Enter k:" << endl;
    get_num(k);
    cout << "Enter b:" << endl;
    get_num(b);
    tr.set_line(k, b);
    return 1;
}

int SetLen(Tractrix &tr) {
    double len;
    cout << "Enter len:" << endl;
    get_num(len);
    try {
        tr.set_length(len);
    }
    catch (exception &br) {
        cout << br.what() << endl;
    }
    return 1;
}

int GetAsympt(Tractrix &tr) {
    cout << "y = " << tr.get_line().k << "x + " << tr.get_line().b << endl;
    return 1;
}

int GetLength(Tractrix &tr) {
    cout << "Your length is " << tr.get_length() << endl;
    return 1;
}

int GetCoordRotate(Tractrix &tr) {
    cout << "Enter angle:" << endl;
    double angle;
    Point p;
    try {
        angle = get_angle();
        p = tr.get_coordinates(angle);
        cout << "( " << p.x << " ; " << p.y << " )" << endl;
    }
    catch (exception &br) {
        cout << br.what() << endl;
    }
    return 1;
}

int GetLenRotate(Tractrix &tr) {
    cout << "Enter angle:" << endl;
    double len, angle;
    try {
        angle = get_angle();
        len = tr.get_line_length(angle);
        cout << "Your length is " << len << endl;
    }
    catch (exception &br) {
        cout << br.what() << endl;
    }
    return 1;
}

int GetRad(Tractrix &tr) {
    double rad, angle;
    cout << "Enter angle" << endl;
    try {
        angle = get_angle();
        rad = tr.get_radius(angle);
        cout << "Your radius is " << rad << endl;
    }
    catch (exception &br) {
        cout << br.what() << endl;
    }
    return 1;
}

int Get2Area(Tractrix &tr) {
    cout << "Your 2D area is " << tr.get_area() << endl;
    return 1;
}

int Get3Area(Tractrix &tr) {
    cout << "Your 3D area is " << tr.get_rotation_area() << endl;
    return 1;
}

int GetVolume(Tractrix &tr) {
    cout << "Your volume is " << tr.get_volume() << endl;
    return 1;
}


