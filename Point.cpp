#include "Point.h"
#include "Functions.h"
#include "Tractrix.h"

inline double Point::get_x() const {
    return x;
}

inline double Point::get_y() const {
    return y;
}

inline void Point::set_x(double x1) {
    Point::x = x1;
}

inline void Point::set_y(double y1) {
    Point::y = y1;
}

Point Point::operator+(const Point &r) const {
    Point tmp;
    tmp.x = this->x + r.x;
    tmp.y = this->y + r.y;
    return tmp;
}

Point Point::operator-(const Point &r) const {
    Point tmp;
    tmp.x = this->x - r.x;
    tmp.y = this->y - r.y;
    return tmp;
}

Point::Point() {
    Point::x = 0;
    Point::y = 0;
}