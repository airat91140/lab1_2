#include "pch.h"
#include "../lab1_2/Tractrix.h"
#include "../lab1_2/Functions.h"
#include "../lab1_2/Tractrix.cpp"
#include "../lab1_2/Functions.cpp"

TEST(TractrixConstructor, DefaultConstructor)
{
	Tractrix a1;
	ASSERT_EQ(1, a1.get_length());
	ASSERT_EQ(0, a1.get_line().k);
	ASSERT_EQ(0, a1.get_line().b);
}
TEST(TractrixConstructor, InitConstructors)
{
	Tractrix a2(3);
	ASSERT_EQ(3, a2.get_length());
	ASSERT_EQ(0, a2.get_line().k);
	ASSERT_EQ(0, a2.get_line().b);
	Tractrix a3(2, 8);
	ASSERT_EQ(1, a3.get_length());
	ASSERT_EQ(2, a3.get_line().k);
	ASSERT_EQ(8, a3.get_line().b);
	Tractrix a4(4, 5, 3);
	ASSERT_EQ(4, a4.get_length());
	ASSERT_EQ(5, a4.get_line().k);
	ASSERT_EQ(3, a4.get_line().b);
}
TEST(TractrixConstructor, TestException)
{
	ASSERT_ANY_THROW(Tractrix(-2, 5, -5));
	ASSERT_ANY_THROW(Tractrix(0, 5, -5));
	ASSERT_ANY_THROW(Tractrix(0));
	ASSERT_ANY_THROW(Tractrix(-1));
}

TEST(Tractrixset, TestSet)
{
	Tractrix a;
	a.set_length(12);
	ASSERT_EQ(12, a.get_length());
	a.set_line(3, 5);
	ASSERT_EQ(3, a.get_line().k);
	ASSERT_EQ(5, a.get_line().b);
}

TEST(Tractrixset, TestSetexceptions)
{
	Tractrix a;
	ASSERT_ANY_THROW(a.set_length(-12));
	ASSERT_ANY_THROW(a.set_length(0));
}

TEST(TractrixFunctions, TestFunctions)
{
	Tractrix a;
	double t = 1.0;
	ASSERT_NEAR(-0.06428, a.get_coordinates(t).x, 0.001);
	ASSERT_NEAR( 0.84147, a.get_coordinates(t).y, 0.001);
	ASSERT_NEAR(0.99999, a.get_line_length(t), 0.001);
	ASSERT_NEAR(0.64209, a.get_radius(t), 0.001);
	ASSERT_NEAR(PI / 2, a.get_area(), 0.001);
	ASSERT_NEAR(PI * 4, a.get_rotation_area(), 0.001);
	ASSERT_NEAR(2 * PI / 3, a.get_volume(), 0.001);
	a.set_line(3, 2);
	Point p = a.get_coordinates(t);
	ASSERT_NEAR(-1.10893, p.x, 0.001);
	ASSERT_NEAR(2.24648, p.y, 0.001);
	ASSERT_NEAR(3.37679, a.get_line_length(t), 0.001);
	ASSERT_NEAR(0.642, a.get_radius(t), 0.001);
}

TEST(TractrixFunctions, TestExcepttions)
{
	Tractrix a;
	double t1 = -1;
	double t2 = 4;
	ASSERT_ANY_THROW(a.get_coordinates(t1));
	ASSERT_ANY_THROW(a.get_coordinates(t2));
	ASSERT_ANY_THROW(a.get_line_length(t1));
	ASSERT_ANY_THROW(a.get_line_length(t2));
	ASSERT_ANY_THROW(a.get_radius(t1));
	ASSERT_ANY_THROW(a.get_radius(t2));
}
